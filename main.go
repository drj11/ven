package main

import (
	"fmt"
	"os"
	"sort"
)

func main() {
	es := os.Environ()
	sort.Strings(es)
	for _, ev := range es {
		fmt.Println(ev)
	}
}
